# Context
<!-- Discuss here what kinds of documentation is necessary for the subsystem, both for the developers *of* the subsystem and for developers *using* the subsystem.  They should include READMEs, interfaces, and other documents. -->

# Guidelines
<!-- If needed, describe how to translate the above needs into a concrete list of documents (e.g. how to find the list of unit tests to be documented) -->

## READMEs
<!-- The Coding guidelines: https://tezos.gitlab.io/developer/guidelines.html contain a template of README file. Refer to it and add here further indications specific to this subsystem, if any. -->

## Interfaces
<!-- The Coding guidelines: https://tezos.gitlab.io/developer/guidelines.html contains rules for documenting interfaces. Refer to it and add here further indications specific to this subsystem, if any. -->

## Other docs
<!-- Specify how to fill in the standalone documentation mentioned in section Context. -->

# Scope
<!-- Give here the exhaustive list of documents to be produced, split according to the categories above. This list should be associated to concrete tasks for producing these documents, taking the form of MRs or sub-issues. -->

## READMEs
<!-- List of READMEs & the corresponding task(s) to produce them. -->

## Interfaces
<!-- List interface (.mli) files & the corresponding task(s) to produce them. -->

## Other docs
<!-- List of standalone documents & the corresponding task(s) to produce them. -->

# Priorities
<!-- This section may specify priorities for the tasks defined above, in different forms: general rules, ordered list of tasks, table with priority classes, etc. -->
